﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 9:06:08 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;

namespace TunaTK.Editor
{
    [CustomEditor(typeof(MappedInput))]
    public class MappedInputEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Reset Input Mappings"))
            {
                if (EditorUtility.DisplayDialog("Reset Input Mappings", "Are you sure you would like to reset the saved input mappings?\n\nNOTE: this will not reset the defaults, only the saved settings.", "Reset", "Cancel"))
                {
                    PlayerPrefs.DeleteKey((target as MappedInput).MappingSettingKey);
                }
            }
        }
    }
}
