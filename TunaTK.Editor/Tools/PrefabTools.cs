﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/20/2018 8:24:35 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;

namespace TunaTK.Editor
{
    public class PrefabTools
    {
        [MenuItem("Tools/Prefab/Apply Selected")]
        public static void ApplyPrefabs()
        {
            ApplyRevertPrefabs(true);
        }

        [MenuItem("Tools/Prefab/Revert Selected")]
        public static void RevertPrefabs()
        {
            ApplyRevertPrefabs(false);
        }

        private static void ApplyRevertPrefabs(bool aDoApply)
        {
            var objects = Selection.gameObjects;
            var count = 0;

            foreach (var go in objects)
            {
                var type = PrefabUtility.GetPrefabType(go);
                if (type == PrefabType.PrefabInstance || type == PrefabType.DisconnectedPrefabInstance)
                {
                    var root = ((GameObject)PrefabUtility.GetPrefabParent(go)).transform.root.gameObject;
                    var current = go;
                    var topHeirarchyFound = false;
                    var canApply = true;

                    while (current.transform.parent != null && !topHeirarchyFound)
                    {
                        if (PrefabUtility.GetPrefabParent(current.transform.parent.gameObject) != null &&
                            root == ((GameObject)PrefabUtility.GetPrefabParent(current.transform.parent.gameObject)).transform.root.gameObject)
                        {
                            current = current.transform.parent.gameObject;
                        }
                        else
                        {
                            topHeirarchyFound = true;
                            if (root != ((GameObject)PrefabUtility.GetPrefabParent(current)))
                            {
                                canApply = false;
                            }
                        }
                    }

                    if (canApply)
                    {
                        ++count;

                        if (aDoApply)
                        {
                            PrefabUtility.ReplacePrefab(current, PrefabUtility.GetPrefabParent(current), ReplacePrefabOptions.ConnectToPrefab);
                        }
                        else
                        {
                            PrefabUtility.ReconnectToLastPrefab(current);
                            PrefabUtility.RevertPrefabInstance(current);
                        }
                    }
                }
            }
        }
    }
}
