﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/15/2018 8:17:31 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace TunaTK.Editor
{
    public class FindTools
    {
        #region ScriptableObject

        [MenuItem("CONTEXT/ScriptableObject/Ping", false, 400)]
        private static void PingScriptableObject(MenuCommand aData)
        {
            Object context = aData.context;
            if (context)
            {
                EditorGUIUtility.PingObject(context);
            }
        }

        #endregion

        #region Component

        [MenuItem("CONTEXT/Component/Ping", false, 400)]
        private static void PingObject(MenuCommand aData)
        {
            Object context = aData.context;
            if (context)
            {
                if (typeof(Component).IsAssignableFrom(context.GetType()))
                {
                    var go = (context as Component).gameObject;
                    EditorGUIUtility.PingObject(go);
                }
                else
                {
                    EditorGUIUtility.PingObject(context);
                }
            }
        }

        [MenuItem("CONTEXT/Component/Find references to this", false, 410)]
        private static void FindReferences(MenuCommand aData)
        {
            Object context = aData.context;
            if (context)
            {
                var component = context as Component;
                if (component)
                {
                    FindReferencesTo(component);
                }
            }
        }

        private static void FindReferencesTo(Component aTo)
        {
            var referenceBy = new List<Object>();
            var allObjects = Object.FindObjectsOfType<GameObject>();
            for (int j = 0; j < allObjects.Length; j++)
            {
                var go = allObjects[j];

                if (PrefabUtility.GetPrefabType(go) == PrefabType.PrefabInstance)
                {
                    if (PrefabUtility.GetPrefabParent(go) == aTo)
                    {
                        Debug.Log(string.Format("Referenced by {0}, {1}", go.name, go.GetType()), go);
                        referenceBy.Add(go);
                    }
                }

                var components = go.GetComponents<Component>();
                for (int i = 0; i < components.Length; i++)
                {
                    var component = components[i];
                    if (!component)
                    {
                        continue;
                    }

                    var serializedObject = new SerializedObject(component);
                    var serializedProp = serializedObject.GetIterator();

                    while (serializedProp.NextVisible(true))
                    {
                        if (serializedProp.propertyType == SerializedPropertyType.ObjectReference)
                        {
                            if (serializedProp.objectReferenceValue == aTo)
                            {
                                Debug.Log(string.Format("Referenced by {0}, {1}", component.name, component.GetType()), component);
                                referenceBy.Add(component.gameObject);
                            }
                        }
                    }
                }
            }

            if (referenceBy.Count > 0)
            {
                Selection.objects = referenceBy.ToArray();
            }
            else
            {
                Debug.Log("No references in scene.");
            }
        }

        #endregion
    }
}
