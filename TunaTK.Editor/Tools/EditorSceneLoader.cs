﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/15/2018 7:40:23 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

namespace TunaTK.Editor
{
    static class EditorSceneLoader
    {
        private const string SHORTCUT = "%0";

        private const string PREVIOUS_SCENE_PATH_KEY = "EditorSceneLoader.PreviousScene";
        private static string mPreviousScenePath
        {
            get { return EditorPrefs.GetString(PREVIOUS_SCENE_PATH_KEY, "none"); }
            set { EditorPrefs.SetString(PREVIOUS_SCENE_PATH_KEY, value); }
        }

        private const string STARTED_BY_SHORTCUT_KEY = "EditorSceneLoader.StartedByShortcut";
        private static bool mStartedByShortcut
        {
            get { return EditorPrefs.GetBool(STARTED_BY_SHORTCUT_KEY, false); }
            set { EditorPrefs.SetBool(STARTED_BY_SHORTCUT_KEY, value); }
        }

        private static string CurrentScene
        {
            get
            {
                Scene activeScene = SceneManager.GetActiveScene();
                string result;
                if (activeScene.IsValid())
                {
                    result = activeScene.path;
                }
                else
                {
                    result = "";
                }

                return result;
            }
        }

        static EditorSceneLoader()
        {
            EditorApplication.playModeStateChanged += ReloadIfPlayModeHasStopped;
        }

        private static void ReloadIfPlayModeHasStopped(PlayModeStateChange aState)
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                if (!mStartedByShortcut && TunaTKEditorSettings.UseStarterScene)
                {
                    PlayFromMasterScene();
                }
                return;
            }

            if (!mStartedByShortcut)
                return;

            if (!EditorApplication.isPlaying)
            {
                EditorSceneManager.OpenScene(mPreviousScenePath);
                mStartedByShortcut = false;
            }
        }

        [MenuItem("Tunacorn Studios/Play " + SHORTCUT, false, 1)]
        public static void PlayFromMasterScene()
        {
            if (!EditorApplication.isPlaying)
            {
                if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                    return;

                mPreviousScenePath = CurrentScene;
                EditorSceneManager.OpenScene(Application.dataPath + TunaTKEditorSettings.StarterScenePath.Substring("Assets".Length));
                mStartedByShortcut = true;
                EditorApplication.isPlaying = true;
            }
            else
            {
                EditorApplication.isPlaying = false;
            }
        }
    }
}
