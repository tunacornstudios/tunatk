﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 10:13:33 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;

namespace TunaTK.Editor
{
    public partial class TunaTKSettingsEditor : EditorWindow
    {
        [MenuItem("Window/TunaTK Settings Manager", priority = 1000)]
        public static void Init()
        {
            EditorWindow window = EditorWindow.GetWindow(typeof(TunaTKSettingsEditor));
            window.titleContent = new GUIContent("TunaTK");
        }

        private void RenderLogo()
        {
            Texture logo = Resources.Load<Texture>("TunaTK-Logo");

            RenderTextureToGUI(logo, 20, 50, 50);

            minSize = new Vector2(logo.width / 20, logo.height / 20);
        }

        private void RenderTextureToGUI(Texture aTexture, float aDivisionFactor = 2f, float aBufferX = 0f, float aBufferY = 0f)
        {
            Vector2 size = new Vector2((aTexture.width + aBufferX) / aDivisionFactor, (aTexture.height + aBufferY) / aDivisionFactor);

            GUI.DrawTexture(GUILayoutUtility.GetAspectRect(size.x / size.y), aTexture);
        }

        private void OnGUI()
        {
            RenderLogo();

            TunaTKEditorSettings.SelectedTab = GUILayout.Toolbar(TunaTKEditorSettings.SelectedTab, new string[] { "General", "Editor Scene Loader", "Input Mapper", "Script Template" });

            switch (TunaTKEditorSettings.SelectedTab)
            {
                case 0:
                    OnGeneralTab();
                    break;
                case 1:
                    OnEditorSceneLoaderTab();
                    break;
                case 2:
                    OnInputMapperTab();
                    break;
                case 3:
                    OnScriptDefineTab();
                    break;
                default:
                    break;
            }
        }
    }
}
