﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 10:27:28 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;

namespace TunaTK.Editor
{
    public partial class TunaTKSettingsEditor : EditorWindow
    {
        private bool mIsRunning = false;

        private void OnEditorSceneLoaderTab()
        {
            TunaTKEditorSettings.UseStarterScene = EditorGUILayout.Toggle("Use Starter Scene", TunaTKEditorSettings.UseStarterScene);

            GUI.enabled = TunaTKEditorSettings.UseStarterScene;

            var oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(TunaTKEditorSettings.StarterScenePath);
            var scene = EditorGUILayout.ObjectField("Master Scene", oldScene, typeof(SceneAsset), false) as SceneAsset;
            TunaTKEditorSettings.StarterScenePath = AssetDatabase.GetAssetPath(scene);

            if (GUILayout.Button(mIsRunning ? "Stop" : "Play"))
            {
                mIsRunning = !mIsRunning;
                EditorSceneLoader.PlayFromMasterScene();
            }

            GUI.enabled = false;
        }
    }
}
