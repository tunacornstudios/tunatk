﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 10:29:08 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;

namespace TunaTK.Editor
{
    public partial class TunaTKSettingsEditor : EditorWindow
    {
        public void OnGeneralTab()
        {
            TunaTKSettings.UseExtendedLogging = EditorGUILayout.Toggle("Use Extended Logging", TunaTKSettings.UseExtendedLogging);
        }
    }
}
