﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/27/2018 10:12:15 AM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;

namespace TunaTK.Editor
{
    public partial class TunaTKSettingsEditor : EditorWindow
    {
        public void OnScriptDefineTab()
        {
            EditorGUILayout.LabelField("Document Header");
            TunaTKEditorSettings.DocumentHeader = EditorGUILayout.TextArea(TunaTKEditorSettings.DocumentHeader);

            EditorGUILayout.Space();

            TunaTKEditorSettings.OverrideUsername = EditorGUILayout.Toggle("Override Username", TunaTKEditorSettings.OverrideUsername);
            GUI.enabled = TunaTKEditorSettings.OverrideUsername;
            TunaTKEditorSettings.Username = EditorGUILayout.TextField("Username", TunaTKEditorSettings.Username);
            GUI.enabled = true;

            EditorGUILayout.Space();

            TunaTKEditorSettings.HideModuleNamespace = EditorGUILayout.Toggle("Hide Module Namespace", TunaTKEditorSettings.HideModuleNamespace);
            TunaTKEditorSettings.HideEditorNamespace = EditorGUILayout.Toggle("Hide Editor Namespace", TunaTKEditorSettings.HideEditorNamespace);

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("Add keywords that you would like to remove from namespace paths separated by a comma (No Spaces).\n\nFor Example: 'The,Cow'.", EditorStyles.wordWrappedLabel);
            EditorGUILayout.EndVertical();
            TunaTKEditorSettings.KeywordsToRemove = EditorGUILayout.TextArea(TunaTKEditorSettings.KeywordsToRemove);
        }
    }
}