﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/14/2018 10:13:23 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;

namespace TunaTK.Editor
{
    [CustomPropertyDrawer(typeof(SceneAssetFieldAttribute))]
    public class SceneAssetFieldPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect aPosition, SerializedProperty aProperty, GUIContent aLabel)
        {
            var oldPath = AssetDatabase.LoadAssetAtPath<SceneAsset>(aProperty.stringValue);

            aPosition = EditorGUI.PrefixLabel(aPosition, new GUIContent(aProperty.displayName));

            EditorGUI.BeginChangeCheck();
            var newScene = EditorGUI.ObjectField(aPosition, oldPath, typeof(SceneAsset), false) as SceneAsset;

            if (EditorGUI.EndChangeCheck())
            {
                var newPath = AssetDatabase.GetAssetPath(newScene);
                aProperty.stringValue = newPath;
            }
        }
    }
}
