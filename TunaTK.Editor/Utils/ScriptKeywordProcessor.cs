﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 13/11/2018 10:00 PM
// Created On: CHRONOS

using System;
using System.IO;
using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace TunaTK.Editor
{
    internal sealed class ScriptKeywordProcessor : UnityEditor.AssetModificationProcessor
    {
        [MenuItem("Assets/Create/Tunacorn Studios/C# Script", priority = 20)]
        public static void CreateBehaviourScript()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/BehaviourScriptTemplate");

            CreateScriptAsset(template, "NewBehaviourScript");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Network Script", priority = 21)]
        public static void CreateNetworkBehaviourScript()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/NetworkBehaviourScriptTemplate");

            CreateScriptAsset(template, "NewNetworkBehaviour");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Scriptable Object", priority = 22)]
        public static void CreateScriptableObject()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/ScriptableObjectTemplate");

            CreateScriptAsset(template, "NewScriptableObject");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Singleton Script", priority = 23)]
        public static void CreateBehaviourSingleton()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/BehaviourSingletonTemplate");

            CreateScriptAsset(template, "NewBehaviourSingleton");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Class", priority = 40)]
        public static void CreateClassScript()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/ClassScriptTemplate");

            CreateScriptAsset(template, "NewClass");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Enum", priority = 41)]
        public static void CreateEnumScript()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/EnumScriptTemplate");

            CreateScriptAsset(template, "NewEnum");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Interface", priority = 41)]
        public static void CreateInterfaceScript()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/InterfaceScriptTemplate");

            CreateScriptAsset(template, "INewInterface");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Initializable", priority = 60)]
        public static void CreateInitializableScript()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/InitializableScriptTemplate");

            CreateScriptAsset(template, "NewInitializableScript");
        }

        [MenuItem("Assets/Create/Tunacorn Studios/C# Initializable Singleton", priority = 61)]
        public static void CreateInitializableSingleton()
        {
            TextAsset template = Resources.Load<TextAsset>("Script Templates/InitializableSingletonTemplate");

            CreateScriptAsset(template, "NewInitializableSingleton");
        }

        private static void CreateScriptAsset(TextAsset aAsset, string aFileName)
        {
            typeof(ProjectWindowUtil).GetMethod("CreateScriptAsset", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] { AssetDatabase.GetAssetPath(aAsset), AssetDatabase.GetAssetPath(Selection.activeObject) + "/" + aFileName + ".cs" });
        }

        public static void OnWillCreateAsset(string aPath)
        {
            aPath = aPath.Replace(".meta", "");
            int index = aPath.LastIndexOf(".");
            if (index < 0)
                return;

            string file = aPath.Substring(index);
            if (file != ".cs" && file != ".js")
                return;

            index = Application.dataPath.LastIndexOf("Assets");
            aPath = Application.dataPath.Substring(0, index) + aPath;
            if (!File.Exists(aPath))
                return;

            string fileContent = File.ReadAllText(aPath);

            fileContent = fileContent.Replace("#HEADER#", TunaTKEditorSettings.DocumentHeader);
            fileContent = fileContent.Replace("#CREATE_NAME#", TunaTKEditorSettings.OverrideUsername ? TunaTKEditorSettings.Username : GetDisplayname());
            fileContent = fileContent.Replace("#CREATE_DATE#", DateTime.Now.ToString("dd/MM/yyyy"));
            fileContent = fileContent.Replace("#CREATE_TIME#", DateTime.Now.ToString("hh:mm tt"));
            fileContent = fileContent.Replace("#CREATE_MACHINE#", Environment.MachineName);
            fileContent = fileContent.Replace("#NAMESPACE#", GetNamespaceFromPath(aPath));

            File.WriteAllText(aPath, fileContent);
            AssetDatabase.Refresh();
        }

        private static string GetDisplayname()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(UnityEditor.EditorWindow));
            object uc = assembly.CreateInstance("UnityEditor.Connect.UnityConnect", false, BindingFlags.NonPublic | BindingFlags.Instance, null, null, null, null);
            // Cache type of UnityConnect.
            Type t = uc.GetType();
            // Get user info object from UnityConnect.
            var userInfo = t.GetProperty("userInfo").GetValue(uc, null);
            // Retrieve user id from user info.
            Type userInfoType = userInfo.GetType();

            return userInfoType.GetProperty("displayName").GetValue(userInfo, null) as string;
        }

        private static string GetSafeNamespace(string aNamespace)
        {
            string safeNamespace = aNamespace;
            safeNamespace = RemoveCharacter(safeNamespace, ' ');
            safeNamespace = RemoveCharacter(safeNamespace, '-');

            return safeNamespace;
        }

        private static string RemoveCharacter(string aStringToChange, char aCharToRemove)
        {
            string toChange = aStringToChange;
            int index = toChange.IndexOf(aCharToRemove);

            while (index != -1)
            {
                toChange = toChange.Remove(index, 1);
                index = toChange.IndexOf(aCharToRemove);
            }

            return toChange;
        }

        private static string GetNamespaceFromPath(string aPath)
        {
            int index = aPath.LastIndexOf("Assets") + ("Assets").Length;
            string localPath = aPath.Substring(index, aPath.Length - index);

            string newNamespace = localPath.Replace('/', '.');
            newNamespace = newNamespace.Replace("Scripts.", "");
            newNamespace = newNamespace.Replace("Code.", "");

            if (TunaTKEditorSettings.HideEditorNamespace)
            {
                newNamespace = newNamespace.Replace("Editor.", "");
            }

            if (TunaTKEditorSettings.HideModuleNamespace)
            {
                newNamespace = newNamespace.Replace("Modules.", "");
            }

            string[] keywords = TunaTKEditorSettings.KeywordsToRemove.Split(',');
            foreach (var keyword in keywords)
            {
                newNamespace = newNamespace.Replace(keyword, "");
            }

            newNamespace = newNamespace.Substring(1);
            newNamespace = newNamespace.Substring(0, newNamespace.Length - 3);
            if (newNamespace.IndexOf('.') != -1)
            {
                newNamespace = newNamespace.Substring(0, newNamespace.LastIndexOf('.'));
            }
            else
            {
                newNamespace = Application.productName;
            }

            return GetSafeNamespace(newNamespace);
        }
    }
}