﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 10:09:03 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace TunaTK.Editor
{
    public static class TunaTKEditorSettings
    {
        #region Setting Keys

        private const string LIBRARY_KEY = "TunaTK";

        // ------------- EDITOR SCENE LOADER ------------- //
        private const string STARTER_SCENE_PATH_KEY = LIBRARY_KEY + ".EditorSceneLoader.StarterScene";
        private const string USE_STARTER_SCENE_KEY = LIBRARY_KEY + ".EditorSceneLoader.UseStarterScene";
        // ------------- EDITOR SCENE LOADER ------------- //

        // ------------- TUNATK SETTINGS MANAGER ------------- //
        private const string SELECTED_TAB_KEY = LIBRARY_KEY + ".SettingsMenu.SelectedTab";
        // ------------- TUNATK SETTINGS MANAGER ------------- //

        // ------------- SCRIPT DEFINE SYSTEM ------------- //
        private const string DOCUMENT_HEADER_KEY = LIBRARY_KEY + ".ScriptDefineSettings.DocumentHeader";
        private const string OVERRIDE_USERNAME_KEY = LIBRARY_KEY + ".ScriptDefineSettings.OverrideUsername";
        private const string USERNAME_KEY = LIBRARY_KEY + ".ScriptDefineSettings.Username";
        private const string HIDE_MODULE_NAMESPACE_KEY = LIBRARY_KEY + ".ScriptDefineSettings.HideModuleNamespace";
        private const string HIDE_EDITOR_NAMESPACE_KEY = LIBRARY_KEY + ".ScriptDefineSettings.HideEditorNamespace";
        private const string KEYWORDS_TO_REMOVE_KEY = LIBRARY_KEY + ".ScriptDefineSettings.KeywordsToRemove";
        // ------------- SCRIPT DEFINE SYSTEM ------------- //

        #endregion

        // ------------- EDITOR SCENE LOADER ------------- //
        public static string StarterScenePath
        {
            get { return EditorPrefs.GetString(STARTER_SCENE_PATH_KEY, "none"); }
            set { EditorPrefs.SetString(STARTER_SCENE_PATH_KEY, value); }
        }

        public static bool UseStarterScene
        {
            get { return EditorPrefs.GetBool(USE_STARTER_SCENE_KEY, false); }
            set { EditorPrefs.SetBool(USE_STARTER_SCENE_KEY, value); }
        }
        // ------------- EDITOR SCENE LOADER ------------- //

        // ------------- TUNATK SETTINGS MANAGER ------------- //
        public static int SelectedTab
        {
            get { return EditorPrefs.GetInt(SELECTED_TAB_KEY, 0); }
            set { EditorPrefs.SetInt(SELECTED_TAB_KEY, value); }
        }
        // ------------- TUNATK SETTINGS MANAGER ------------- //

        // ------------- SCRIPT DEFINE SYSTEM ------------- //
        public static string DocumentHeader
        {
            get { return EditorPrefs.GetString(DOCUMENT_HEADER_KEY, "// Property of TUNACORN STUDIOS PTY LTD 2018\n//\n// Creator: #CREATE_NAME#\n// Creation Time: #CREATE_DATE# #CREATE_TIME#\n// Created On: #CREATE_MACHINE#"); }
            set { EditorPrefs.SetString(DOCUMENT_HEADER_KEY, value); }
        }

        public static bool OverrideUsername
        {
            get { return EditorPrefs.GetBool(OVERRIDE_USERNAME_KEY, false); }
            set { EditorPrefs.SetBool(OVERRIDE_USERNAME_KEY, value); }
        }

        public static string Username
        {
            get { return EditorPrefs.GetString(USERNAME_KEY, ""); }
            set { EditorPrefs.SetString(USERNAME_KEY, value); }
        }

        public static bool HideModuleNamespace
        {
            get { return EditorPrefs.GetBool(HIDE_MODULE_NAMESPACE_KEY, true); }
            set { EditorPrefs.SetBool(HIDE_MODULE_NAMESPACE_KEY, value); }
        }

        public static bool HideEditorNamespace
        {
            get { return EditorPrefs.GetBool(HIDE_EDITOR_NAMESPACE_KEY, true); }
            set { EditorPrefs.SetBool(HIDE_EDITOR_NAMESPACE_KEY, value); }
        }

        public static string KeywordsToRemove
        {
            get { return EditorPrefs.GetString(KEYWORDS_TO_REMOVE_KEY, ""); }
            set { EditorPrefs.SetString(KEYWORDS_TO_REMOVE_KEY, value); }
        }
        // ------------- SCRIPT DEFINE SYSTEM ------------- //
    }
}
