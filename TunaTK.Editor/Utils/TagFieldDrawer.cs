﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 13/11/2018 10:00 PM
// Created On: CHRONOS

using UnityEditor;
using UnityEngine;

namespace TunaTK.Editor
{
    [CustomPropertyDrawer(typeof(TagFieldAttribute))]
    public class TagFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            bool isNotSet = false;
            if (string.IsNullOrEmpty(property.stringValue))
                isNotSet = true;

            property.stringValue = EditorGUI.TagField(position, label, isNotSet ? (property.serializedObject.targetObject as Component).gameObject.tag : property.stringValue);

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }
    }
}
