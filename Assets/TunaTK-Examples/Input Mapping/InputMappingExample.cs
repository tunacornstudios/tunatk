// Property of TUNACORN STUDIOS PTY LTD 2018
//
// Creator: James Mills
// Creation Time: 22/11/2018 09:27 PM
// Created On: CHRONOS

using TunaTK;
using UnityEngine;

namespace TunaTKExamples.InputMapping
{
	public class InputMappingExample : MonoBehaviour 
	{
        public KeyCode ToMapTo = KeyCode.Space;

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                MappedInput.AddMapping("Jump", ToMapTo);
            }
        }
    }
}