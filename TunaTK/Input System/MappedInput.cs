﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/21/2018 10:35:39 PM
// Created On: CHRONOS

using UnityEngine;
using System.Collections.Generic;

namespace TunaTK
{
    public class MappedInput : MonoBehaviour
    {
        private static MappedInput mInstance = null;

        public string MappingSettingKey { get { return string.Format("{0}.InputMappings", Application.productName); } }
        
        [SerializeField]
        private InputMappingsList m_DefaultMappings = new InputMappingsList();
        
        private Dictionary<string, KeyCode> mMappings = new Dictionary<string, KeyCode>();
        private InputMappingsList mInputMappings = new InputMappingsList();
        
        private void Awake()
        {
            if (mInstance == null)
            {
                mInstance = this;
            }
            else if (mInstance != null)
            {
                Debug.LogErrorFormat("Duplicate MappedInput component detected on [{0}]... Removing...", gameObject.name);
                Destroy(this);
                return;
            }

            DontDestroyOnLoad(gameObject);

            LoadMappings();
        }

        private void OnDestroy()
        {
            if (mInstance == this)
            {
                SaveMappings();
            }
        }

        private void LoadMappings()
        {
            string json = Settings.GetValue<string>(MappingSettingKey, JsonUtility.ToJson(m_DefaultMappings)) as string;
            mInputMappings = JsonUtility.FromJson<InputMappingsList>(json);

            foreach (var mapping in mInputMappings.Mappings)
            {
                mMappings.Add(mapping.ID, mapping.Value);
            }
        }

        private void SaveMappings()
        {
            mInputMappings.UpdateMappings(mMappings);

            string json = JsonUtility.ToJson(mInputMappings);
            Settings.SaveValue<string>(MappingSettingKey, json);
        }

        /// <summary>
        /// Remaps a mapping to the newly specified value if the mapping exists, if it doesn't, it will add it to the mappings.
        /// </summary>
        public static void Remap(string aMappingID, KeyCode aNewValue)
        {
            if (mInstance.mMappings.ContainsKey(aMappingID))
            {
                mInstance.mMappings[aMappingID] = aNewValue;
                return;
            }

            if (TunaTKSettings.UseExtendedLogging)
            {
                Debug.LogFormat("No mapping with ID [{0}] found... Will attempt to add instead...", aMappingID);
            }
            AddMapping(aMappingID, aNewValue);
        }

        /// <summary>
        /// Adds a mapping to the mapping dictionary, if it does already exist, it will be updated instead.
        /// </summary>
        public static void AddMapping(string aMappingID, KeyCode aValue)
        {
            if (!mInstance.mMappings.ContainsKey(aMappingID))
            {
                mInstance.mMappings.Add(aMappingID, aValue);
                return;
            }

            if (TunaTKSettings.UseExtendedLogging)
            {
                Debug.LogFormat("Mapping with ID [{0}] already exists... Updating instead...", aMappingID);
            }
            Remap(aMappingID, aValue);
        }

        /// <summary>
        /// Returns whether or not the mapping with the specified ID has just been pressed down this frame.
        /// </summary>
        public static bool IsMappingDown(string aMappingID)
        {
            if (mInstance.mMappings.ContainsKey(aMappingID))
            {
                return Input.GetKeyDown(mInstance.mMappings[aMappingID]);
            }

            Debug.LogErrorFormat("No mapping with ID [{0}] found... Please check default values on MappedInput instance [{1}]...", aMappingID, mInstance.gameObject.name);
            return false;
        }

        /// <summary>
        /// Returns whether or not the mapping with the specified ID is not held down.
        /// </summary>
        public static bool IsMappingUp(string aMappingID)
        {
            if (mInstance.mMappings.ContainsKey(aMappingID))
            {
                return Input.GetKeyUp(mInstance.mMappings[aMappingID]);
            }

            Debug.LogErrorFormat("No mapping with ID [{0}] found... Please check default values on MappedInput instance [{1}]...", aMappingID, mInstance.gameObject.name);
            return false;
        }

        /// <summary>
        /// Returns whether or not the mapping with the specified ID is held down.
        /// </summary>
        public static bool IsMappingPressed(string aMappingID)
        {
            if (mInstance.mMappings.ContainsKey(aMappingID))
            {
                return Input.GetKey(mInstance.mMappings[aMappingID]);
            }

            Debug.LogErrorFormat("No mapping with ID [{0}] found... Please check default values on MappedInput instance [{1}]...", aMappingID, mInstance.gameObject.name);
            return false;
        }

        /// <summary>
        /// Returns whether or not the mapping actually exists.
        /// </summary>
        public static bool DoesMappingExist(string aMappingID)
        {
            return mInstance.mMappings.ContainsKey(aMappingID);
        }
    }
}
