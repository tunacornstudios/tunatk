﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 8:18:36 PM
// Created On: CHRONOS

using UnityEngine;
using System.Collections.Generic;
using Serializable = System.SerializableAttribute;

namespace TunaTK
{
    [Serializable]
    public class InputMappingsList
    {
        public InputMapping[] Mappings;

        public void UpdateMappings(Dictionary<string, KeyCode> aNewMappings)
        {
            List<InputMapping> mappings = new List<InputMapping>();

            foreach (var mapping in aNewMappings)
            {
                mappings.Add(new InputMapping(mapping.Key, mapping.Value));
            }

            Mappings = mappings.ToArray();
        }
    }
}
