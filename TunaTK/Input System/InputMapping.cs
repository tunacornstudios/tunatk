﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 8:08:51 PM
// Created On: CHRONOS

using UnityEngine;
using Serializable = System.SerializableAttribute;

namespace TunaTK
{
    [Serializable]
    public class InputMapping
    {
        public string ID;
        public KeyCode Value;

        public InputMapping(string aID, KeyCode aValue)
        {
            ID = aID;
            Value = aValue;
        }
    }
}
