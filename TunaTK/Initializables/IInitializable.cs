﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 14/11/2018 09:47 PM
// Created On: CHRONOS

namespace TunaTK
{
    public interface IInitializable
    {
        bool Initialized { get; }
        void Initialize();
    }
}
