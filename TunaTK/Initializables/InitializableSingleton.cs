﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/14/2018 10:04:40 PM
// Created On: CHRONOS

using UnityEngine;

namespace TunaTK
{
    public abstract class InitializableSingleton<T> : Initializable where T : InitializableSingleton<T>
    {
        public static T Instance
        {
            get
            {
                if (mInstance == null)
                {
                    mInstance = FindObjectOfType<T>();

                    if (mInstance == null)
                    {
                        Debug.LogError("Internal '" + typeof(T).Name + "' singleton instance was null, auto creating...");
                        mInstance = new GameObject(typeof(T).Name + " Singleton").AddComponent<T>();
                    }
                }

                return mInstance;
            }
        }

        private static T mInstance = null;

        protected virtual T CreateSingletonInstance()
        {
            mInstance = FindObjectOfType<T>();

            if (mInstance == null)
            {
                Debug.LogError("Internal '" + typeof(T).Name + "' singleton instance was null, auto creating...");
                mInstance = new GameObject(typeof(T).Name + " Singleton").AddComponent<T>();
            }

            return mInstance;
        }

        protected void FlagAsPersistant()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
