﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/14/2018 10:00:00 PM
// Created On: CHRONOS

using UnityEngine;

namespace TunaTK
{
    public abstract class Initializable : MonoBehaviour, IInitializable
    {
        public bool Initialized
        {
            get;
            protected set;
        }

        [SerializeField]
        private bool m_AutoInitialize = false;
        
        [SerializeField]
        private InitializationStage m_InitializationStage = InitializationStage.Awake;

        public void Initialize()
        {
            if (Initialized)
            {
                Debug.LogWarning(string.Format("Initializable object [{0}] already initialized! Ignoring call....", gameObject.name));
                return;
            }

            OnInitialization();

            Initialized = true;
        }

        protected abstract void OnInitialization();

        /// <summary>
        /// Only called once every frame after the object has been initialized.
        /// </summary>
        protected virtual void InitializedUpdate() { }

        // Called before OnEnable and Start
        private void Awake()
        {
            if (m_AutoInitialize && m_InitializationStage == InitializationStage.Awake)
            {
                Initialize();
            }
        }

        // Called before Start and everytime the object becomes active
        private void OnEnable()
        {
            if (m_AutoInitialize && m_InitializationStage == InitializationStage.OnEnable)
            {
                Initialize();
            }
        }

        // Called last in the init chain
        private void Start()
        {
            if (m_AutoInitialize && m_InitializationStage == InitializationStage.Start)
            {
                Initialize();
            }
        }

        // Called once every frame.
        protected virtual void Update()
        {
            if (Initialized)
            {
                InitializedUpdate();
            }
        }
    }
}
