﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/14/2018 10:12:31 PM
// Created On: CHRONOS

using UnityEngine;

namespace TunaTK
{
    public class SceneAssetFieldAttribute : PropertyAttribute
    {
        public static string LoadableName(string aPath)
        {
            string start = "Assets/";
            string end = ".unity";

            if (aPath.EndsWith(end))
            {
                aPath = aPath.Substring(0, aPath.LastIndexOf(end));
            }

            if (start.StartsWith(start))
            {
                aPath = aPath.Substring(start.Length);
            }

            return aPath;
        }
    }
}
