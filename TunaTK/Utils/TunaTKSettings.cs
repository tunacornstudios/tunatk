﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 9:30:25 PM
// Created On: CHRONOS

using UnityEngine;

namespace TunaTK
{
    public static class TunaTKSettings
    {
        #region Setting Keys

        private const string EXTENDED_LOGGING_KEY = "TunaTK.UseExtendedLogging";

        #endregion

        public static bool UseExtendedLogging
        {
            get
            {
                return PlayerPrefs.GetInt(EXTENDED_LOGGING_KEY, 1) == 1;
            }

            set
            {
                PlayerPrefs.SetInt(EXTENDED_LOGGING_KEY, value ? 1 : 0);
                PlayerPrefs.Save();
            }
        }
    }
}
