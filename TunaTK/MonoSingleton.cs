﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 13/11/2018 10:00 PM
// Created On: CHRONOS

using UnityEngine;

namespace TunaTK
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        public static T Instance
        {
            get
            {
                if (mInstance == null)
                {
                    mInstance = FindObjectOfType<T>();

                    if (mInstance == null)
                    {
                        Debug.LogError("Internal '" + typeof(T).Name + "' singleton instance was null, auto creating...");
                        mInstance = new GameObject(typeof(T).Name + " Singleton").AddComponent<T>();
                    }
                }

                return mInstance;
            }
        }

        private static T mInstance = null;

        protected virtual T CreateSingletonInstance()
        {
            mInstance = FindObjectOfType<T>();

            if (mInstance == null)
            {
                Debug.LogError("Internal '" + typeof(T).Name + "' singleton instance was null, auto creating...");
                mInstance = new GameObject(typeof(T).Name + " Singleton").AddComponent<T>();
            }

            return mInstance;
        }

        protected void FlagAsPersistant()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
