// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:03:33 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace TunaTK.UGUITabs
{
    public class Tab : Selectable, IPointerClickHandler
    {
        [System.Serializable]
        public class TabClickEvent : UnityEvent { }

        public int Index { get { return mIndex; } }
        public TabClickEvent OnClick { get { return m_OnClick; } }
        public GameObject Content { get { return m_Content; } }

        [SerializeField]
        private GameObject m_Content;
        [SerializeField]
        private TabClickEvent m_OnClick = new TabClickEvent();

        private TabGroup mTabGroup;
        private int mIndex;

        private List<TabEffect> mTabEffects;
        private List<TabContentEffect> mTabContentEffects;
        private CanvasGroup mCanvasGroup;

        public void Initialize(TabGroup aTabGroup, int aIndex)
        {
            mTabGroup = aTabGroup;
            mIndex = aIndex;

            mTabEffects = new List<TabEffect>(gameObject.GetComponents<TabEffect>());
            if (mTabEffects != null)
            {
                foreach (var tabEffect in mTabEffects)
                {
                    tabEffect.Initialize(mTabGroup, this);
                }
            }

            mTabContentEffects = new List<TabContentEffect>(m_Content.GetComponents<TabContentEffect>());
            if (mTabContentEffects != null)
            {
                foreach (var contentEffects in mTabContentEffects)
                {
                    contentEffects.Initialize(mTabGroup, this);
                }
            }

            mCanvasGroup = m_Content.GetComponent<CanvasGroup>();

            m_OnClick.AddListener(() => { mTabGroup.CurrentTabIndex = mIndex; });
        }

        protected void OnTabGainFocus()
        {
            if (mTabEffects != null)
            {
                foreach (var tabEffect in mTabEffects)
                {
                    tabEffect.OnTabGainFocus();
                }
            }

            if (mTabContentEffects != null)
            {
                foreach (var contentEffect in mTabContentEffects)
                {
                    contentEffect.OnTabGainFocus();
                }
            }

            mCanvasGroup.blocksRaycasts = true;
            interactable = false;
        }

        protected void OnTabLostFocus()
        {
            if (mTabEffects != null)
            {
                foreach (var tabEffect in mTabEffects)
                {
                    tabEffect.OnTabLostFocus();
                }
            }

            if (mTabContentEffects != null)
            {
                foreach (var contentEffect in mTabContentEffects)
                {
                    contentEffect.OnTabLostFocus();
                }
            }

            mCanvasGroup.blocksRaycasts = false;
            interactable = true;
        }

        public void OnPointerClick(PointerEventData aEventData)
        {
            if (aEventData.button != PointerEventData.InputButton.Left)
                return;

            Press();
        }

        private void Press()
        {
            if (!IsActive() || !IsInteractable())
                return;

            UISystemProfilerApi.AddMarker("Tab.OnClick", this);
            m_OnClick.Invoke();
        }
    }
}
