// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:10:48 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEngine.EventSystems;

namespace TunaTK.UGUITabs
{
    public class TabScrollHandler : UIBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private bool mIsHovered = false;
        private TabGroup mTabGroup;

        public void Initialize(TabGroup aTabGroup)
        {
            mTabGroup = aTabGroup;
        }

        public void OnPointerEnter(PointerEventData aEventData)
        {
            mIsHovered = true;
        }

        public void OnPointerExit(PointerEventData aEventData)
        {
            mIsHovered = false;
        }

        private void Update()
        {
            if (mIsHovered)
            {
                if (Input.mouseScrollDelta.y != 0)
                {
                    int direction = Mathf.RoundToInt(Mathf.Sign(Input.mouseScrollDelta.y));
                    mTabGroup.CurrentTabIndex -= direction;

                    if (mTabGroup.CurrentTabIndex >= mTabGroup.TabCount)
                    {
                        mTabGroup.CurrentTabIndex = mTabGroup.TabCount - 1;
                    }
                    else if (mTabGroup.CurrentTabIndex < 0)
                    {
                        mTabGroup.CurrentTabIndex = 0;
                    }
                }
            }
        }
    }
}
