// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:08:13 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEngine.UI;

namespace TunaTK.UGUITabs
{
    [RequireComponent(typeof(LayoutElement))]
    public class TabScaler : TabEffect
    {
        public override void OnTabHover()
        {
            if (mTab.IsInteractable())
            {
                ApplyScale(mTabGroup.ActiveTabScaling.x * mTabGroup.HoverScaleMultiplier, mTabGroup.ActiveTabScaling.y * mTabGroup.HoverScaleMultiplier);
            }
        }

        public override void OnTabUnhover()
        {
            if (mTab.IsInteractable())
            {
                ResetScale();
            }
        }

        public override void OnTabGainFocus()
        {
            ApplyScale(mTabGroup.ActiveTabScaling.x, mTabGroup.ActiveTabScaling.y);
        }

        public override void OnTabLostFocus()
        {
            ResetScale();

            if (mHovered)
            {
                OnTabHover();
            }
        }

        #region Scaling Functions
        public void ResetScale()
        {
            ApplyScale(0);
        }

        public void ApplyScale(float aAddedScale)
        {
            ApplyScale(aAddedScale, aAddedScale);
        }

        public void ApplyScale(float aAddedWidth, float aAddedHeight)
        {
            mLayoutElement.preferredWidth = mTabGroup.BasePreferredSize.x + aAddedWidth;
            mLayoutElement.preferredHeight = mTabGroup.BasePreferredSize.y + aAddedHeight;
        }
        #endregion
    }
}
