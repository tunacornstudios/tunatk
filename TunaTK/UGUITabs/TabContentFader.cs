// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:10:10 PM
// Created On: CHRONOS

using UnityEngine;
using System.Collections;

namespace TunaTK.UGUITabs
{
    public class TabContentFader : TabContentEffect
    {
        [SerializeField]
        private float m_TransitionTime = 1f;

        public override void OnTabGainFocus()
        {
            StartCoroutine(RunAlphaTransition_CR(0));
        }

        public override void OnTabLostFocus()
        {
            StartCoroutine(RunAlphaTransition_CR(1));
        }

        private IEnumerator RunAlphaTransition_CR(float aTargetAlpha)
        {
            float timer = 0;

            while (timer < m_TransitionTime)
            {
                mCanvasGroup.alpha = Mathf.Lerp(1 - aTargetAlpha, aTargetAlpha, Mathf.Clamp01(timer / m_TransitionTime));

                yield return null;

                timer += Time.deltaTime;
            }
        }
    }
}
