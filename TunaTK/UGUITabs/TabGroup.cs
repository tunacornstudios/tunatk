// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:06:15 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace TunaTK.UGUITabs
{
    public class TabGroup : UIBehaviour
    {
        public Tab SelectedTab { get { return mCurrentTab; } }
        public Vector2 BasePreferredSize { get; private set; }
        public int TabCount { get { return mTabs.Count; } }
        [HideInInspector]
        public int CurrentTabIndex = 0;

        [SerializeField]
        [Tooltip("")]
        private RectTransform m_TabHolder;
        [SerializeField]
        [Tooltip("")]
        private bool m_CacheSelectedTab = false;
        [Tooltip("")]
        public Vector2 ActiveTabScaling = Vector2.one;
        [Range(0, 1f)]
        [Tooltip("")]
        public float HoverScaleMultiplier = 0.5f;
        [SerializeField]
        private TabScrollHandler m_ScrollHandler;

        private List<Tab> mTabs = new List<Tab>();
        private Tab mCurrentTab;

        private int mPreviousTabIndex = 0;

        private string mTabPlayerPrefsKey = "";

        // Use this for initialization
        protected override void Start()
        {
            mTabPlayerPrefsKey = "tabgroup-" + name.ToLower().Replace(" ", "") + "-selected-index";

            if (m_TabHolder == null)
            {
                m_TabHolder = transform as RectTransform;
            }

            if (m_ScrollHandler != null)
            {
                m_ScrollHandler.Initialize(this);
            }

            LocateTabs();
            ActivateInitialTab();
        }

        protected override void OnDestroy()
        {
            if (m_CacheSelectedTab)
            {
                PlayerPrefs.SetInt(mTabPlayerPrefsKey, CurrentTabIndex);
                PlayerPrefs.Save();
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (CurrentTabIndex != mPreviousTabIndex)
            {
                SendTabLostFocusMessage(mCurrentTab);

                mCurrentTab = mTabs[CurrentTabIndex];
                SendTabGainFocusMessage(mCurrentTab);
            }

            mPreviousTabIndex = CurrentTabIndex;
        }

        private void LocateTabs()
        {
            int index = 0;
            foreach (Transform child in m_TabHolder)
            {
                Tab tab = child.GetComponent<Tab>();

                if (tab != null)
                {
                    if (!tab.gameObject.activeSelf)
                    {
                        tab.Content.SetActive(false);
                        continue;
                    }

                    if (index == 0)
                    {
                        LayoutElement element = tab.GetComponent<LayoutElement>();
                        BasePreferredSize = new Vector2(element.preferredWidth, element.preferredHeight);
                    }

                    tab.Initialize(this, index++);
                    SendTabLostFocusMessage(tab);
                    mTabs.Add(tab);
                }
            }
        }

        private void ActivateInitialTab()
        {
            if (m_CacheSelectedTab)
            {
                CurrentTabIndex = PlayerPrefs.GetInt(mTabPlayerPrefsKey, 0);
                if (CurrentTabIndex >= mTabs.Count)
                {
                    CurrentTabIndex = mTabs.Count - 1;
                }
            }
            mCurrentTab = mTabs[CurrentTabIndex];
            SendTabGainFocusMessage(mCurrentTab);
        }

        private void SendTabGainFocusMessage(Tab aTab)
        {
            aTab.SendMessage("OnTabGainFocus", SendMessageOptions.DontRequireReceiver);
        }

        private void SendTabLostFocusMessage(Tab aTab)
        {
            aTab.SendMessage("OnTabLostFocus", SendMessageOptions.DontRequireReceiver);
        }
    }
}
