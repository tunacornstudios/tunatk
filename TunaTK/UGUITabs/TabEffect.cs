// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:07:26 PM
// Created On: CHRONOS

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace TunaTK.UGUITabs
{
    [RequireComponent(typeof(LayoutElement))]
    public abstract class TabEffect : UIBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        protected LayoutElement mLayoutElement;
        protected TabGroup mTabGroup;
        protected Tab mTab;
        protected bool mHovered = false;

        public void Initialize(TabGroup aTabGroup, Tab aTab)
        {
            mLayoutElement = gameObject.GetComponent<LayoutElement>();
            mTabGroup = aTabGroup;
            mTab = aTab;
        }

        public abstract void OnTabHover();
        public abstract void OnTabUnhover();
        public abstract void OnTabGainFocus();
        public abstract void OnTabLostFocus();

        public void OnPointerEnter(PointerEventData aEventData)
        {
            mHovered = true;
            OnTabHover();
        }

        public void OnPointerExit(PointerEventData aEventData)
        {
            mHovered = false;
            OnTabUnhover();
        }
    }
}
