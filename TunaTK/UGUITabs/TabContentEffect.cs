// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:08:58 PM
// Created On: CHRONOS

using UnityEngine;

namespace TunaTK.UGUITabs
{
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class TabContentEffect : MonoBehaviour
    {
        protected CanvasGroup mCanvasGroup;
        protected TabGroup mTabGroup;
        protected Tab mTab;

        public void Initialize(TabGroup aTabGroup, Tab aTab)
        {
            mCanvasGroup = gameObject.GetComponent<CanvasGroup>();

            mTabGroup = aTabGroup;
            mTab = aTab;
        }

        public abstract void OnTabGainFocus();
        public abstract void OnTabLostFocus();
    }
}
