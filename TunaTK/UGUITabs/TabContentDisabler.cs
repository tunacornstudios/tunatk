// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/16/2018 5:09:51 PM
// Created On: CHRONOS

namespace TunaTK.UGUITabs
{
    public class TabContentDisabler : TabContentEffect
    {
        public override void OnTabGainFocus()
        {
            gameObject.SetActive(true);
        }

        public override void OnTabLostFocus()
        {
            gameObject.SetActive(false);
        }
    }
}
