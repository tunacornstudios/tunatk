﻿// Property of TUNACORN STUDIOS PTY LTD 2018
// 
// Creator: James Mills
// Creation Time: 11/22/2018 8:28:21 PM
// Created On: CHRONOS

using System;
using UnityEngine;

namespace TunaTK
{
    public class Settings
    {
        /// <summary>
        /// Saves a specific value to player preferences. The value type can be any of the following: integer, boolean, string, float or enum
        /// </summary>
        /// <param name="aKey">The key to save the value to preferences under.</param>
        public static void SaveValue<T>(string aKey, T aValue)
        {
            if (typeof(T) == typeof(int))
            {
                PlayerPrefs.SetInt(aKey, Convert.ToInt32(aValue));
            }
            else if (typeof(T) == typeof(bool))
            {
                PlayerPrefs.SetInt(aKey, Convert.ToBoolean(aValue) ? 1 : 0);
            }
            else if (typeof(T) == typeof(string))
            {
                PlayerPrefs.SetString(aKey, Convert.ToString(aValue));
            }
            else if (typeof(T) == typeof(float))
            {
                PlayerPrefs.SetFloat(aKey, Convert.ToSingle(aValue));
            }
            else if (typeof(T) == typeof(Enum))
            {
                PlayerPrefs.SetString(aKey, Convert.ToString(aValue));
            }

            PlayerPrefs.Save();
        }

        /// <summary>
        /// Gets a specific value from the player preferences. The value type can be any of the following: integer, boolean, string, float or enum
        /// </summary>
        /// <param name="aKey">The key to get the value from preferences under.</param>
        public static object GetValue<T>(string aKey, object aDefaultValue)
        {
            if (typeof(T) == typeof(int))
            {
                return PlayerPrefs.GetInt(aKey, Convert.ToInt32(aDefaultValue));
            }
            else if (typeof(T) == typeof(bool))
            {
                return PlayerPrefs.GetInt(aKey, Convert.ToBoolean(aDefaultValue) ? 1 : 0) == 1;
            }
            else if (typeof(T) == typeof(string))
            {
                return PlayerPrefs.GetString(aKey, Convert.ToString(aDefaultValue));
            }
            else if (typeof(T) == typeof(float))
            {
                return PlayerPrefs.GetFloat(aKey, Convert.ToSingle(aDefaultValue));
            }
            else if (typeof(T) == typeof(Enum))
            {
                return Enum.Parse(typeof(T), PlayerPrefs.GetString(aKey, Convert.ToString(aDefaultValue)));
            }

            return null;
        }
    }
}
